#![feature(test)]
extern crate test;

use std::str::FromStr;
use std::fmt::{Display, Formatter, LowerHex};

#[derive(PartialEq, Debug, Clone)]
pub struct BigUInt {
    data: Vec<u64>
}
impl BigUInt {
    pub fn new() -> BigUInt {
        Self { data: vec![0] }
    }
}
impl From<i64> for BigUInt {
    fn from(int: i64) -> Self {
        Self { data: vec![int as u64] }
    }
}
impl From<i32> for BigUInt {
    fn from(int: i32) -> Self {
        Self { data: vec![int as u64] }
    }
}
impl From<Vec<u64>> for BigUInt {
    fn from(data: Vec<u64>) -> Self {
        Self { data }
    }
}
impl FromStr for BigUInt {
    type Err = String;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut data = Vec::with_capacity(input.len() / 20);
        let mut value = 0u64;
        for c in input.chars() {
            value = match value.checked_mul(10) {
                Some(v) => v,
                None => {
                    println!("Pushing value to vec 0x{:x?}", value);
                    data.push(value);
                    0
                }
            };
            match c {
                '0'...'9' => value += ((c as u8) - 0x30) as u64,
                _ => Err(format!("Invalid character: {}", c))?
            }
        }
        if value != 0 { data.push(value) }
        Ok(Self { data } )
    }
}
impl Display for BigUInt {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "")
    }
}
impl LowerHex for BigUInt {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        for (i, d) in self.data.iter().rev().enumerate() {
            let mut s = format!("{:x}", d);
            if i != 0 {
                let mut v = Vec::with_capacity(16);
                (0..16 - s.len()).for_each(|_| v.push(b'0'));
                s = format!("{}{}", String::from_utf8(v).unwrap(), s);
            }
            write!(f, "{}", s)?;
        }
        Ok(())
    }
}
impl<'a, 'b> std::ops::Add<&'b BigUInt> for &'a BigUInt {
    type Output = BigUInt;

    fn add(self, rhs: &'b BigUInt) -> Self::Output {
        let max_len = std::cmp::max(
            self.data.len(), rhs.data.len()
        );
        let data = Vec::with_capacity(
            max_len + 1
        );
        let mut ans = Self::Output { data };
        let mut carry: u64 = 0;
        for i in 0..max_len {
            let (mut digit, overflow) = self.data.get(i).unwrap_or(&0)
                            .overflowing_add(*rhs.data.get(i).unwrap_or(&0));
            debug_assert!(carry <= 1);
            let (digit, overflow) = match digit.overflowing_add(carry) {
                (n, false) => (n, overflow),
                r => r
            };
            carry = if overflow { 1 } else { 0 };
            ans.data.push(digit);
        }
        if carry == 1 { ans.data.push(carry) };
        ans
    }
}

fn vec_add(vec: &mut Vec<u64>, n: u64, i: usize) {
    let mut overflow = true;
    let mut carry = n;
    let mut i = i;

    while overflow {
        while i >= vec.len() {
            vec.push(0)
        }
        let (digit, o2) = vec[i].overflowing_add(carry);
        vec[i] = digit;

        overflow = o2;
        carry = 1;
        i += 1;
    }
}

impl<'a, 'b> std::ops::Mul<&'b BigUInt> for &'a BigUInt {
    type Output = BigUInt;

    fn mul(self, rhs: &'b BigUInt) -> Self::Output {
        // Grade school multiply O(n^2)
        let mut data = Vec::new();
        for (i, d1) in self.data.iter().enumerate() {
            for (i2, d2) in rhs.data.iter().enumerate() {
                let index = i.checked_add(i2).expect("Result index overflowed");
                let index2 = index.checked_add(1).expect("Result index overflowed");
                let (d3, carry) = wide_mul(*d1, *d2);
                vec_add(&mut data, d3, index);

                if carry > 0 {
                    vec_add(&mut data, carry, index2);
                }
            }
        }
        Self::Output { data }
    }
}

impl BigUInt {
    fn mul_karatsuba(&self, y: &BigUInt) -> BigUInt {
        let (xlen, ylen) = (self.data.len(), y.data.len());
        if xlen == 1 && ylen == 1 {
            let (d, carry) = wide_mul(self.data[0], y.data[0]);
            let mut data = Vec::with_capacity(2);
            data.push(d);
            if carry != 0 { data.push(carry); }
            return BigUInt { data };
        }
        let (lox, hix) = (BigUInt::from(self.data[0..xlen/2].to_vec()), BigUInt::from(self.data[xlen/2..xlen].to_vec()));
        let (loy, hiy) = (BigUInt::from(y.data[0..xlen/2].to_vec()), BigUInt::from(y.data[xlen/2..xlen].to_vec()));

        println!("(lox, hix) ({:?}, {:?})", lox, hix);
        let z0 = lox.mul_karatsuba(&loy);
        let mut z1 = (&lox + &hix).mul_karatsuba(&(&loy + &hiy));
        let mut z2 = hix.mul_karatsuba(&hiy);

        let mut data = Vec::with_capacity(xlen + ylen);
        data.append(&mut z1.data); // needs to be z1 - z2 - z0
        data.append(&mut z2.data);
        let result = BigUInt{ data };
        println!("result {:?}", result);
        println!("z0 {:?}", z0);
        &result + &z0
    }
}

fn wide_mul(x: u64, y: u64) -> (u64, u64) {
    let z = x as u128 * y as u128;
    ((z % 18446744073709551616 as u128) as u64, (z >> 64 as u128) as u64)
}


#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench] fn bench_add(b: &mut Bencher) {
        let x = BigUInt{ data: vec![u64::max_value(); 20]};
        let y = BigUInt{ data: vec![u32::max_value() as u64; 21]};
        b.iter(
            || &x + &y
        );
    }

    #[bench] fn bench_mul(b: &mut Bencher) {
        let x = BigUInt{ data: vec![u64::max_value(); 20]};
        let y = BigUInt{ data: vec![u32::max_value() as u64; 21]};
        b.iter(
            || &x * &y
        );
    }

    #[test] fn basics() {
        let a: BigUInt = BigUInt::new();
        let _ = a.clone();
    }

    #[test] fn from() {
        let _: BigUInt = BigUInt::from(1i64);
        let _: BigUInt = BigUInt::from(1i32);
    }

    #[test] fn from_vec() {
        let _: BigUInt = BigUInt::from(vec![0, 0, 1]);
    }

    #[test] fn parse_implemented() {
        assert_eq!(BigUInt { data: vec![1] }, "1".parse::<BigUInt>().expect("small positive"));
        assert_eq!(BigUInt { data: vec![1234567890] }, "1234567890".parse::<BigUInt>().expect("big positive"));
        assert_eq!(BigUInt { data: vec![12446928571455179474, 11585827206506214328, 3] }, "1234567890123456789012345678901234567890".parse::<BigUInt>().expect("big positive"));
    }

    #[test] fn partial_eq() {
        assert_eq!(BigUInt { data: vec![123] }, BigUInt { data: vec![123] })
    }

    fn assert_fmt(s: &str) {
        let b: BigUInt = s.parse().unwrap();
        assert_eq!(format!("{}", b), s);
    }
    #[test] fn display_1() {
        assert_fmt("1");
        assert_fmt("12345");
        assert_fmt("8652985908267259876509822786576549389873265");
        assert_fmt("865298590826723453509822786576549389873265");
    }
    #[test] fn display_2() {
        assert_eq!(format!("{}", BigUInt { data: vec![12345] }), "12345");
        assert_eq!(format!("{}", BigUInt { data: vec![6558190101741922864, 129340779167] }), "2385916251594391276165385906736");
    }
    #[test] fn display_3() {
        assert_eq!(format!("{:x}", BigUInt { data: vec![12345] }), "3039");
        assert_eq!(format!("{:x}",
            BigUInt { data: vec![6558190101741922864, 129340779167] }),
            "1e1d4faa9f5b035f2697cf6a30");
        assert_eq!(format!("{:x}",
            BigUInt { data: vec![4404228639012311521, 331967548131889466, 315429207697368] }),
            "11ee195e067d8049b62b9e75bd13a3d1ef69cd1ee51e1");
        assert_eq!(format!("{:x}",
            BigUInt { data: vec![12349678, 32450, 435634560, 1] }),
            "10000000019f741800000000000007ec20000000000bc70ee");
    }

    #[test] fn add_1 () {
        let x = BigUInt { data: vec![5] };
        let y = BigUInt { data: vec![5] };
        assert_eq!(BigUInt { data: vec![10] }, &x + &y);
        let _ = &x + &y;
    }

    #[test] fn add_2 () {
        assert_eq!(BigUInt { data: vec![17179869184] },
            &BigUInt { data: vec![8589934592] } + &BigUInt { data: vec![8589934592] }
        );
    }

    #[test] fn add_3 () {
        let ans = BigUInt { data: vec![0, 2] };
        let result = &BigUInt { data: vec![0, 1] } +
                     &BigUInt { data: vec![0, 1] };
        assert_eq!(ans, result);
    }

    #[test] fn add_4() {
        let ans = BigUInt { data: vec![0, 0, 3] };
        let result = &BigUInt { data: vec![0, 0, 1] } +
                     &BigUInt { data: vec![0, 0, 2] };
        assert_eq!(ans, result);
    }

    #[test] fn add_5() {
        let ans = BigUInt { data: vec![0, 0, 2, 1] };
        let result = &BigUInt { data: vec![0, 0, 0, 1] } +
                     &BigUInt { data: vec![0, 0, 2] };
        assert_eq!(ans, result);
    }

    #[test] fn add_6() {
        let ans = BigUInt { data: vec![u64::max_value() - 1, 1] };
        let result = &BigUInt { data: vec![u64::max_value()] } +
                     &BigUInt { data: vec![u64::max_value()] };
        assert_eq!(ans, result);
    }

    #[test] fn add_7() {
        let ans = BigUInt { data: vec![0, 1] };
        let result = &BigUInt { data: vec![u64::max_value()] } +
                     &BigUInt { data: vec![1] };
        assert_eq!(ans, result);
    }

    #[test] fn add_8() {
        let ans = BigUInt { data: vec![0, 0, 1] };
        let result = &BigUInt { data: vec![u64::max_value(), u64::max_value() - 1] } +
                     &BigUInt { data: vec![1, 1] };
        assert_eq!(ans, result);
    }

    #[test] fn mul_1 () {
        let x = BigUInt { data: vec![5] };
        let y = BigUInt { data: vec![5] };
        assert_eq!(BigUInt { data: vec![25] }, &x * &y);
        let _ = &x * &y;
    }

    #[test] fn mul_2 () {
        let ans = BigUInt { data: vec![0, 0, 1] };
        let result = &BigUInt { data: vec![0, 1] } *
                     &BigUInt { data: vec![0, 1] };
        assert_eq!(ans, result);
    }

    #[test] fn mul_3 () {
        let ans = BigUInt { data: vec![0, 0, 4] };
        let result = &BigUInt { data: vec![0, 2] } *
                     &BigUInt { data: vec![0, 2] };
        assert_eq!(ans, result);
    }

    #[test] fn mul_4 () {
        let ans = BigUInt { data: vec![u64::max_value() - 1, 1] };
        let result = &BigUInt { data: vec![u64::max_value()] } *
                     &BigUInt { data: vec![2] };
        assert_eq!(ans, result);
    }

    #[test] fn mul_5 () {
        let ans = BigUInt { data: vec![1, 18446744073709551614] };
        let result = &BigUInt { data: vec![u64::max_value()] } *
                     &BigUInt { data: vec![u64::max_value()] };
        assert_eq!(ans, result);
    }

    #[test] fn mul_6 () {
        let ans = BigUInt { data: vec![6548554139465249258, 1761537699298725771, 17367317421655622271, 88940262527821434] };
        let result = &BigUInt { data: vec![17816662882775757038, 281] } *
                     &BigUInt { data: vec![5034309829946106099, 331967548131889184, 315429207697368] };
        assert_eq!(ans, result);
    }
    #[test] fn mul_k1 () {
        let x = BigUInt { data: vec![5] };
        let y = BigUInt { data: vec![5] };
        assert_eq!(BigUInt { data: vec![25] }, BigUInt::mul_karatsuba(&x, &y));
    }

    #[test] fn mul_k2 () {
        let ans = BigUInt { data: vec![0, 0, 1] };
        let result = BigUInt::mul_karatsuba(&BigUInt { data: vec![0, 1] },
                     &BigUInt { data: vec![0, 1] });
        assert_eq!(ans, result);
    }

    #[test] fn mul_k3 () {
        let ans = BigUInt { data: vec![0, 0, 4] };
        let result = BigUInt::mul_karatsuba(&BigUInt { data: vec![0, 2] },
                     &BigUInt { data: vec![0, 2] });
        assert_eq!(ans, result);
    }

    #[test] fn mul_k4 () {
        let ans = BigUInt { data: vec![u64::max_value() - 1, 1] };
        let result = BigUInt::mul_karatsuba(&BigUInt { data: vec![u64::max_value()] },
                     &BigUInt { data: vec![2] });
        assert_eq!(ans, result);
    }

    #[test] fn mul_k5 () {
        let ans = BigUInt { data: vec![1, 18446744073709551614] };
        let result = BigUInt::mul_karatsuba(&BigUInt { data: vec![u64::max_value()] },
                     &BigUInt { data: vec![u64::max_value()] });
        assert_eq!(ans, result);
    }

    #[test] fn mul_k6 () {
        let ans = BigUInt { data: vec![6548554139465249258, 1761537699298725771, 17367317421655622271, 88940262527821434] };
        let result = BigUInt::mul_karatsuba(&BigUInt { data: vec![17816662882775757038, 281] },
                     &BigUInt { data: vec![5034309829946106099, 331967548131889184, 315429207697368] });
        assert_eq!(ans, result);
    }
}
