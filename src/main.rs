extern crate bigint;

use bigint::BigUInt;

fn main() {
    let mut x = BigUInt::from(vec![12349678, 32450, 435634560, 1]);
    let y = BigUInt::from(vec![123123, 123123, 4578678, 123457568, 1236675486, 3456, 1232145342567]);
    print!("0x{:x} + 0x{:x} = ", x, y);
    println!("0x{:x}\n", &x + &y);
    print!("0x{:x} * 0x{:x} = ", x, y);
    println!("0x{:x}", &x * &y);

    for _ in 0..100 {
        x = &x * &y;
        println!("x = {:x}\n", x);
    }
}
