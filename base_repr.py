import sys


def num_to_digits(num, base):
    biggest = 0
    while True:
        if num / base**biggest < base:
            break
        biggest += 1

    digits = []
    for i in reversed(range(biggest + 1)):
        d = num // base**i
        num %= base**i
        digits.append(d)
    return digits


def digits_to_num(digits, base):
    num = 0
    for digit in digits:
        num *= base
        num += digit
    return num


def interactive():
    base = 2**64
    while True:
        try:
            i = input()
            if i.lower() in ("quit", "q"):
                return
            print(num_to_digits(int(i), base))
        except (EOFError, KeyboardInterrupt):
            return


def main():
    if len(sys.argv) > 1:
        if sys.argv[1] in ("interactive", "-i", "--interactive"):
            interactive()
        else:
            print(num_to_digits(int(sys.argv[1])))
    else:
        base = 2**64
        num = 0x11ee195e067d8049b62b9e75bd13a3d1ef69cd1ee51e1
        print(num)
        digits = num_to_digits(num, base)
        print(digits)
        print(digits_to_num(digits, base))
        print(hex(digits_to_num([1, 435634560, 32450, 12349678], base)))


if __name__ == "__main__":
    main()
